<?php

namespace Drupal\nodehive_core\Render\Element;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Menu\MenuLinkManagerInterface;

/**
 * Adds active trail to trail.
 *
 * * @package Drupal\gin_toolbar\Render\Element.
 */
class NodeHiveToolbar implements TrustedCallbackInterface
{

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks()
  {
    return ['preRenderTray'];
  }

  /**
   * Renders the toolbar's administration tray.
   *
   * This is a clone of core's toolbar_prerender_toolbar_administration_tray()
   * function, which adds active trail information and which uses setMaxDepth(4)
   * instead of setTopLevelOnly() in case the Admin Toolbar module is installed.
   *
   * @param array $build
   *   A renderable array.
   *
   * @return array
   *   The updated renderable array.
   *
   * @see toolbar_prerender_toolbar_administration_tray()
   */
  public static function preRenderTray(array $build)
  {

    $nodehive_admin_tree = [];
    $admin_tree = [];

    $current_user = \Drupal::currentUser();
    $menu_tree_service = \Drupal::service('toolbar.menu_tree');

    if ($current_user->id() == 1 || $current_user->hasRole('nodehive_content_editor')) {
      // Add custom markup to the $build array
      $build['somecontent'] = [
        //'#markup' => '<div>Custom</div>',
      ];


      // Define the parameters for loading the menu tree
      $parameters = new \Drupal\Core\Menu\MenuTreeParameters();
      $parameters->setMaxDepth(3); // Adjust the depth as needed
      $parameters->onlyEnabledLinks();

      // Load the menu tree for 'nodehive-admin'
      $menu_tree = $menu_tree_service->load('nodehive-editor', $parameters);

      // Transform the tree using the default manipulators (you can add or remove manipulators as needed)
      $manipulators = [
        // Use the default manipulators to perform tasks like checking access and sorting the menu links
        ['callable' => 'menu.default_tree_manipulators:checkAccess'],
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
        ['callable' => 'gin_toolbar_tools_menu_navigation_links'],
      ];
      $nodehive_admin_tree = $menu_tree_service->transform($menu_tree, $manipulators);
    }

    if ($current_user->id() == 1 || $current_user->hasRole('nodehive_content_admin')) {
      // Add custom markup to the $build array
      $build['somecontent'] = [
        //'#markup' => '<div>Custom</div>',
      ];


      // Define the parameters for loading the menu tree
      $parameters = new \Drupal\Core\Menu\MenuTreeParameters();
      $parameters->setMaxDepth(3); // Adjust the depth as needed
      $parameters->onlyEnabledLinks();

      // Load the menu tree for 'nodehive-admin'
      $menu_tree = $menu_tree_service->load('nodehive-admin', $parameters);

      // Transform the tree using the default manipulators (you can add or remove manipulators as needed)
      $manipulators = [
        // Use the default manipulators to perform tasks like checking access and sorting the menu links
        ['callable' => 'menu.default_tree_manipulators:checkAccess'],
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
        ['callable' => 'gin_toolbar_tools_menu_navigation_links'],
      ];
      $nodehive_admin_tree = $menu_tree_service->transform($menu_tree, $manipulators);
    }


    if ($current_user->id() == 1) {
      $activeTrail = \Drupal::service('gin_toolbar.active_trail')
        ->getActiveTrailIds('admin');
      $parameters = (new MenuTreeParameters())
        ->setActiveTrail($activeTrail)
        ->setRoot('system.admin')
        ->excludeRoot()
        ->setTopLevelOnly()
        ->onlyEnabledLinks();

      if (\Drupal::moduleHandler()->moduleExists('admin_toolbar')) {
        $admin_toolbar_settings = \Drupal::config('admin_toolbar.settings');
        $max_depth = $admin_toolbar_settings->get('menu_depth') ?? 4;
        $parameters->setMaxDepth($max_depth);
      }

      $tree = $menu_tree_service->load('admin', $parameters);
      $manipulators = [
        ['callable' => 'menu.default_tree_manipulators:checkAccess'],
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
        ['callable' => 'gin_toolbar_tools_menu_navigation_links'],
      ];
      $admin_tree = $menu_tree_service->transform($tree, $manipulators);
    }

    $merged_tree = array_merge($nodehive_admin_tree, $admin_tree);

    $build['administration_menu'] = $menu_tree_service->build($merged_tree);
    //$build['#cache']['contexts'][] = 'route.menu_active_trails:admin';
    //$build['administration_menu'] = [];

    return $build;
  }

}
