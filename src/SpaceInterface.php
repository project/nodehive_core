<?php

namespace Drupal\nodehive_core;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a space entity type.
 */
interface SpaceInterface extends ContentEntityInterface, EntityChangedInterface {

}
