<?php

namespace Drupal\nodehive_core\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the space entity edit forms.
 */
class SpaceForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form_id = $form_state->getFormObject()->getFormId();

    $form['nodehive_space_views_container'] = [
      '#type' => 'details',
      '#title' => $this->t("Dashboard Content Views"),
      '#weight' => -2,
    ];

    $form['nodehive_space_views_container'][] = $form['nodehive_space_views'];
    unset($form['nodehive_space_views']);

    if ($form_id == "nodehive_space_add_form") {
      return $form;
    }

    $entity = $form_state->getFormObject()->getEntity();

    $form['saved_apikey'] = [
      '#type' => 'item',
      '#title' => $this->t('API Key'),
      '#markup' => $entity->apikey->value,
      '#weight' => 1,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        // Generate api key.
        $key = bin2hex(random_bytes(32));
        $entity->apikey->value = $key;
        $entity->save();

        $this->messenger()->addStatus($this->t('New space %label has been created.', $message_arguments));
        $this->logger('nodehive_core')->notice('Created new space %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The space %label has been updated.', $message_arguments));
        $this->logger('nodehive_core')->notice('Updated space %label.', $logger_arguments);
        break;
    }

    $tags = $entity->getCacheTags();
    $content = $entity->content->referencedEntities();

    /** @var \Drupal\node\NodeInterface $item */
    foreach ($content as $item) {
      $tags = array_merge($tags, $item->getCacheTags());
    }

    $tags[] = "config:system.menu.nodehive-editor";
    $tags[] = "config:system.menu.nodehive-administrator";
    Cache::invalidateTags($tags);
    $form_state->setRedirect('entity.nodehive_space.canonical', ['nodehive_space' => $entity->id()]);

    return $result;
  }

}
