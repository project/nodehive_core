<?php

namespace Drupal\nodehive_core;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\MenuInterface;

/**
 * Provides an interface for a MenuHelper class.
 */
interface MenuHelperInterface {

  /**
   * Menus that are non-editable for roles that are not administrators.
   *
   * @var array
   */
  public const ADMIN_MENUS = [
    "admin",
    "devel",
    "footer",
    "nodehive-admin",
    "nodehive-editor",
    "tools",
    "account",
  ];

  /**
   * Alters the menu add/edit forms and adds the space reference field.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   */
  public function alterMenuForm(array &$form, FormStateInterface $form_state): void;

  /**
   * Form submit handler for menu form, that saves the space reference field.
   *
   * The field is saved as a third-party setting.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   */
  public function saveMenuForm(array &$form, FormStateInterface $form_state): void;

  /**
   * Get the field values for space reference field from third-party settings.
   *
   * @param \Drupal\system\MenuInterface $menu
   *   Menu entity.
   *
   * @return array
   *   Array with space entity ids as values.
   */
  public function getSpaceFieldValue(MenuInterface $menu): array;

}
