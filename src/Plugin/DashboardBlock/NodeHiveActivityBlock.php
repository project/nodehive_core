<?php

namespace Drupal\nodehive_core\Plugin\DashboardBlock;

use Drupal\content_planner\DashboardBlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a 'MyRecentNodeEditsBlock' block.
 *
 * @DashboardBlock(
 *   id = "nodehive_activity_block",
 *   name = @Translation("Activity"),
 *   category = "NodeHive",
 *   id_category = "nodehive"
 * )
 */
class NodeHiveActivityBlock extends DashboardBlockBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'nodehive-activity-block',
        ],
      ],
    ];

    $build['wrapper']['tabs'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'activity-tabs',
        ],
      ],
    ];

    $build['wrapper']['tabs']['team'] = [
      '#type' => 'button',
      '#value' => $this->t('Team'),
      '#attributes' => [
        'class' => ['button', 'button--small'],
        'data-action' => 'team',
      ],
    ];

    $build['wrapper']['tabs']['assigned_to_me'] = [
      '#type' => 'button',
      '#value' => $this->t('Assigned to me'),
      '#attributes' => [
        'class' => ['button', 'button--small', 'button-assigned-to-me'],
        'data-action' => 'assigned-to-me',
      ],
    ];

    $build['wrapper']['content'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'activity-content',
        ],
      ],
    ];

    $block_configuration = $this->getConfiguration();
    $num_items = $block_configuration['plugin_specific_config']['num_items'] ?? 10;
    $selected_workflow = $block_configuration['plugin_specific_config']['workflow'] ?? 'editorial';

    $build['wrapper']['#attached']['library'] = [
      'nodehive_core/activity-block',
      'nodehive_core/views.entity.translations.links',
    ];

    $build['#attached']['drupalSettings']['nodehiveActivityBlock'] = [
      'numberOfItems' => $num_items,
      'workflow' => $selected_workflow,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigSpecificFormFields(
    FormStateInterface &$form_state,
    Request &$request,
    array $block_configuration,
  ) {
    $form = [];

    // Field to configure the number of items to display.
    $form['num_items'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of Items'),
      '#description' => $this->t('The number of recent edits to display.'),
      '#default_value' => $block_configuration['plugin_specific_config']['num_items'] ?? 10,
      '#min' => 1,
      '#required' => TRUE,
    ];

    // Retrieve all available workflows for the select options.
    $workflows = $this->entityTypeManager
      ->getStorage('workflow')
      ->loadMultiple();

    $workflow_options = array_map(function ($workflow) {
      return $workflow->label();
    }, $workflows);

    // Field to select the workflow.
    $form['workflow'] = [
      '#type' => 'select',
      '#title' => $this->t('Workflow'),
      '#description' => $this->t('Select the workflow.'),
      '#options' => $workflow_options,
      '#default_value' => $block_configuration['plugin_specific_config']['workflow'] ?? 'editorial',
      '#required' => TRUE,
    ];

    return $form;
  }

}
