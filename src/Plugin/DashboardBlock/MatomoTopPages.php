<?php

namespace Drupal\nodehive_core\Plugin\DashboardBlock;

use Drupal\content_planner\DashboardBlockBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides a MatomoTopPages block for Content Planner Dashboard.
 *
 * @DashboardBlock(
 *   id = "matomo_top_pages",
 *   name = @Translation("Matomo Top Pages"),
 *   category = "Site Analytics",
 *   id_category = "site_analytics"
 * )
 */
class MatomoTopPages extends DashboardBlockBase
{
  use StringTranslationTrait;

  /**
   * The HTTP client to fetch the stats.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('http_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {

    $config = $this->getConfiguration();

    if (
      !isset(
      $config['plugin_specific_config']['matomo_url'],
      $config['plugin_specific_config']['site_id'],
      $config['plugin_specific_config']['api_key']
    )
    ) {
      return [
        '#markup' => $this->t('Please configure the Matomo settings in the dashboard block configuration.'),
      ];
    }

    $build['matomotoppages'] = [
      '#type' => 'wire',
      '#id' => 'matomotoppages',
      '#context' => [
        'matomoUrl' => $config['plugin_specific_config']['matomo_url'],
        'siteId' => $config['plugin_specific_config']['site_id'],
        'apiKey' => $config['plugin_specific_config']['api_key'],
      ],
    ];
    return $build;
  }


  /**
   * {@inheritdoc}
   */
  public function getConfigSpecificFormFields(
    FormStateInterface &$form_state,
    Request &$request,
    array $block_configuration
  ) {
    $form = [];

    $form['matomo_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Matomo URL'),
      '#description' => $this->t('The URL of your Matomo instance.'),
      '#default_value' => $block_configuration['plugin_specific_config']['matomo_url'] ?? '',
      '#required' => TRUE,
    ];

    $form['site_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site ID'),
      '#description' => $this->t('The ID of the site in Matomo you want to fetch data for.'),
      '#default_value' => $block_configuration['plugin_specific_config']['site_id'] ?? '',
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('The API key for authentication with Matomo.'),
      '#default_value' => $block_configuration['plugin_specific_config']['api_key'] ?? '',
      '#required' => TRUE,
    ];

    $form['matomo_link'] = [
      '#type' => 'url',
      '#title' => $this->t('Link to Matomo'),
      '#description' => $this->t('The direct link to your Matomo dashboard.'),
      '#default_value' => $block_configuration['plugin_specific_config']['matomo_link'] ?? '',
      '#required' => FALSE,
    ];

    return $form;
  }
}
