<?php

namespace Drupal\nodehive_core\Plugin\DashboardBlock;

use Drupal\content_planner\DashboardBlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a 'NodeHiveQuickLinksBlock' block.
 *
 * @DashboardBlock(
 *   id = "node_hive_quick_links_block",
 *   name = @Translation("NodeHive Quick Links"),
 *   category = "NodeHive",
 *   id_category = "nodehive"
 * )
 */
class NodeHiveQuickLinksBlock extends DashboardBlockBase implements ContainerFactoryPluginInterface
{

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $build = [];



    $quick_links = [
      'invited_teammates' => [
        'title' => $this->t('Invite Teammates'),
        'description' => $this->t('Collaborate with your team members.'),
        'url' => '/nodehive/users',
        'link_text' => 'Manage Users',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M18 7.5v3m0 0v3m0-3h3m-3 0h-3m-2.25-4.125a3.375 3.375 0 1 1-6.75 0 3.375 3.375 0 0 1 6.75 0ZM3 19.235v-.11a6.375 6.375 0 0 1 12.75 0v.109A12.318 12.318 0 0 1 9.374 21c-2.331 0-4.512-.645-6.374-1.766Z" />
      </svg>'

      ],
      'configure_spaces' => [
        'title' => $this->t('Configure Spaces'),
        'description' => $this->t('Configure your spaces and start building.'),
        'url' => '/admin/content/space',
        'link_text' => 'Manage Spaces',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <rect x="2" y="2" width="9" height="9" rx="1" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke-linecap="round" stroke-linejoin="round" d="M2 5h9" />
        <rect x="13" y="2" width="9" height="9" rx="1" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke-linecap="round" stroke-linejoin="round" d="M13 5h9" />
        <rect x="7" y="13" width="9" height="9" rx="1" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke-linecap="round" stroke-linejoin="round" d="M7 16h9" />
      </svg>




      '
      ],
      'documentation' => [
        'title' => $this->t('Documentation'),
        'description' => $this->t('Discover and search through our docs.'),
        'url' => 'https://docs.nodehive.com',
        'link_text' => 'Go to Documentation',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Zm-9 5.25h.008v.008H12v-.008Z" />
      </svg>'
      ],

    ];



    $build = array_merge($build, $this->renderLinks('quick', $quick_links));




    $build['quick_links']['#attached'] = [
      'library' => [
        'nodehive_core/quick_links',
      ],
    ];

    return $build;
  }


  function renderLinks($section, $links)
  {
    $build[$section] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['quick-links', 'quick-links-grid']],
    ];
    foreach ($links as $key => $info) {
      $build[$section][$key] = [
        '#type' => 'fieldset',
        '#title' => $info['title'],
        '#open' => TRUE,
      ];

      $icon_markup = !empty($info['icon']) ? $info['icon'] : '';

      $build[$section][$key]['content'] = [
        '#type' => 'item',
        '#markup' =>
          '<div class="link-content">' .
          '<div class="icon-container">' . $icon_markup . '</div>' .
          '<div>' . $info['description'] . '</div>' .
          '</div>' .
          '<p><a href="' . $info['url'] . '" class="button button--small">' . $info['link_text'] . '</a></p>',

        '#allowed_tags' => ['p', 'a', 'path', 'svg', 'rect', 'div', 'text', 'line', 'circle', 'g', 'title'],
      ];
    }
    return $build;
  }


}
