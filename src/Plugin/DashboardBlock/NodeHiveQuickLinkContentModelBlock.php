<?php

namespace Drupal\nodehive_core\Plugin\DashboardBlock;

use Drupal\content_planner\DashboardBlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a 'NodeHiveQuickLinksContentModelBlock' block.
 *
 * @DashboardBlock(
 *   id = "node_hive_quick_links_content_model_block",
 *   name = @Translation("NodeHive Content Model"),
 *   category = "NodeHive",
 *   id_category = "nodehive"
 * )
 */
class NodeHiveQuickLinkContentModelBlock extends DashboardBlockBase implements ContainerFactoryPluginInterface
{

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $build = [];


    $create_links = [
      'configure_content_types' => [
        'title' => $this->t('Configure Content Types'),
        'description' => $this->t('Configure your custom content types like Page, Article or Event.'),
        'url' => '/admin/structure/types',
        'link_text' => 'Configure Content Types',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <rect x="4" y="4" width="16" height="16" rx="2" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke-linecap="round" stroke-linejoin="round" d="M8 8h8M8 12h5M8 16h3" />
      </svg>
      '
      ],
      'configure_paragraph_types' => [
        'title' => $this->t('Configure Paragraph Types'),
        'description' => $this->t('Configure your paragraph types like a Text Section, Gallery Section or Teaser List.'),
        'url' => '/admin/structure/paragraphs_type',
        'link_text' => 'Configure Paragraph Types',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M4 7h16M4 11h16M4 15h16" />
        <circle cx="18" cy="19" r="2" fill="currentColor" />
      </svg> '
      ],

      'configure_taxonomy' => [
        'title' => $this->t('Configure Taxonomy'),
        'description' => $this->t('Configure your taxonomies like Article categories, Event Types or Blog Post Tags.'),
        'url' => '/admin/structure/taxonomy',
        'link_text' => 'Configure Taxonomies',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
      <path stroke-linecap="round" stroke-linejoin="round" d="M9.568 3H5.25A2.25 2.25 0 0 0 3 5.25v4.318c0 .597.237 1.17.659 1.591l9.581 9.581c.699.699 1.78.872 2.607.33a18.095 18.095 0 0 0 5.223-5.223c.542-.827.369-1.908-.33-2.607L11.16 3.66A2.25 2.25 0 0 0 9.568 3Z" />
      <path stroke-linecap="round" stroke-linejoin="round" d="M6 6h.008v.008H6V6Z" />
    </svg>
    '
      ],
      'configure_fragment_types' => [
        'title' => $this->t('Configure Fragement Types'),
        'description' => $this->t('Configure your fragment types like social media links, global CTAs and reusable content.'),
        'url' => '/admin/structure/fragment_type',
        'link_text' => 'Configure Fragment Types',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M3 10h4v4H3zM17 10h4v4h-4zM10 3h4v4h-4zM10 17h4v4h-4z" />
      </svg>'
      ],


      'configure_area' => [
        'title' => $this->t('Configure Areas'),
        'description' => $this->t('Configure your areas to place fragments in the header, footer or anywhere you need them.'),
        'url' => '/admin/content/area',
        'link_text' => 'Configure Areas',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M6 3h12M6 21h12M3 6v12M21 6v12M6 6h12v12H6z" />
      </svg>'
      ],

      'configure_menus' => [
        'title' => $this->t('Configure Menus'),
        'description' => $this->t('Configure navigation menus with links and attributes.'),
        'url' => '/admin/structure/menu',
        'link_text' => 'Configure Menus',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M3 6h18M3 12h12m-12 6h9" />
      </svg>'
      ]

    ];


    $build = array_merge($build, $this->renderLinks('create', $create_links));


    $build['quick_links']['#attached'] = [
      'library' => [
        'nodehive_core/quick_links',
      ],
    ];

    return $build;
  }


  function renderLinks($section, $links)
  {
    $build[$section] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['quick-links', 'quick-links-grid']],
    ];
    foreach ($links as $key => $info) {
      $build[$section][$key] = [
        '#type' => 'fieldset',
        '#title' => $info['title'],
        '#open' => TRUE,
      ];

      $icon_markup = !empty($info['icon']) ? $info['icon'] : '';

      $build[$section][$key]['content'] = [
        '#type' => 'item',
        '#markup' =>
          '<div class="link-content">' .
          '<div class="icon-container">' . $icon_markup . '</div>' .
          '<div>' . $info['description'] . '</div>' .
          '</div>' .
          '<p><a href="' . $info['url'] . '" class="button button--small">' . $info['link_text'] . '</a></p>',

          '#allowed_tags' => ['div', 'p', 'a', 'svg', 'path', 'rect', 'circle'], // Add more allowed tags as needed.
      ];
    }
    return $build;
  }


}
