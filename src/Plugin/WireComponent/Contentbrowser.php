<?php declare(strict_types = 1);

namespace Drupal\nodehive_core\Plugin\WireComponent;

use Drupal\wire\View;
use Drupal\wire\WireComponent;
use Drupal\Core\Url;

/**
 * Provides "ContentBrowser" Wire component.
 *
 * @WireComponent(
 *   id = "contentbrowser",
 *   label = @Translation("ContentBrowser"),
 * )
 */
class Contentbrowser extends WireComponent {

  public string $search = '';
  public string $contentType = '';

  public function render(): ?View {
    return View::fromTpl('contentbrowser', [
      'items' => $this->getArticles(),
      'content_types' => $this->getContentTypes(),
    ]);
  }

  private function getArticles(): array {
    $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
    $query = $nodeStorage->getQuery()->accessCheck(TRUE)
      ->condition('status', 1)
      ->condition('title', '%' . addcslashes($this->search, '\%_') . '%', 'LIKE')
      ->range(0, 500);

    if (!empty($this->contentType)) {
      $query->condition('type', $this->contentType);
    }

    $articles = $nodeStorage->loadMultiple($query->execute());

    return array_map(function ($article) {
      $nodeTypeStorage = \Drupal::entityTypeManager()->getStorage('node_type');
      $nodeType = $nodeTypeStorage->load($article->bundle());

      return [
        'link' => $article->toLink(),
        'status' => $article->isPublished() ? t('Published') : t('Unpublished'),
        'edit_url' => Url::fromRoute('entity.node.edit_form', ['node' => $article->id()])->toString(),
        'type' => $nodeType ? $nodeType->label() : t('Unknown'),
      ];
    }, $articles);
  }

  private function getContentTypes(): array {
    $contentTypes = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    $options = [];
    foreach ($contentTypes as $type) {
      $options[$type->id()] = $type->label();
    }
    return $options;
  }
}
