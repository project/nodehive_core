<?php declare(strict_types=1);

namespace Drupal\nodehive_core\Plugin\WireComponent;


use Drupal\wire\View;
use Drupal\wire\WireComponent;

/**
 * Implementation for Counter Wire Component.
 *
 * @WireComponent(
 *   id = "counter",
 *   label = @Translation("Counter"),
 * )
 */
class Counter extends WireComponent
{

  public int $count = 0;

  public function increment()
  {
    $this->count++;
  }

  public function render(): ?View
  {
    $twig = <<<'TWIG'
      <div class="">
      <button wire:click="increment">+</button>

      <h1>{{ count }}</h1>
      </div>
    TWIG;

    return View::fromString($twig);
  }

}
