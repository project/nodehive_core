<?php

namespace Drupal\nodehive_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\JsonResponse;

class ContentBrowserController extends ControllerBase
{

  public function contentBrowser()
  {

    $build['counter'] = [
      '#type' => 'wire',
      '#id' => 'contentbrowser',
    ];

    return $build;
  }

}
