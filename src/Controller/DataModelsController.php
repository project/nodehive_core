<?php

namespace Drupal\nodehive_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines DataModelsController class for the NodeHive module.
 */
class DataModelsController extends ControllerBase
{

  /**
   * Display the custom page with NodeHive quick links.
   *
   * @return array
   *   A render array for a Drupal page.
   */
  public function dataModeller()
  {
    $create_links = $this->getQuickLinks();
    $build = $this->renderLinks('create', $create_links);

    // Attach the library for styling and JavaScript if needed.
    $build['#attached']['library'][] = 'nodehive_core/quick_links';

    return $build;
  }

  /**
   * Get the array of quick links to be rendered.
   *
   * @return array
   *   The quick links configuration.
   */
  private function getQuickLinks() {
    $create_links = [
      'configure_content_types' => [
        'title' => $this->t('Configure Content Types'),
        'description' => $this->t('Configure your custom content types like Page, Article or Event.'),
        'url' => '/admin/structure/types',
        'link_text' => 'Configure Content Types',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <rect x="4" y="4" width="16" height="16" rx="2" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke-linecap="round" stroke-linejoin="round" d="M8 8h8M8 12h5M8 16h3" />
      </svg>',
      ],
      'configure_paragraph_types' => [
        'title' => $this->t('Configure Paragraph Types'),
        'description' => $this->t('Configure your paragraph types like a Text Section, Gallery Section or Teaser List.'),
        'url' => '/admin/structure/paragraphs_type',
        'link_text' => 'Configure Paragraph Types',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M4 7h16M4 11h16M4 15h16" />
        <circle cx="18" cy="19" r="2" fill="currentColor" />
        </svg> ',
      ],

      'configure_taxonomy' => [
        'title' => $this->t('Configure Taxonomy'),
        'description' => $this->t('Configure your taxonomies like Article categories, Event Types or Blog Post Tags.'),
        'url' => '/admin/structure/taxonomy',
        'link_text' => 'Configure Taxonomies',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
      <path stroke-linecap="round" stroke-linejoin="round" d="M9.568 3H5.25A2.25 2.25 0 0 0 3 5.25v4.318c0 .597.237 1.17.659 1.591l9.581 9.581c.699.699 1.78.872 2.607.33a18.095 18.095 0 0 0 5.223-5.223c.542-.827.369-1.908-.33-2.607L11.16 3.66A2.25 2.25 0 0 0 9.568 3Z" />
      <path stroke-linecap="round" stroke-linejoin="round" d="M6 6h.008v.008H6V6Z" />
      </svg>',
      ],
      'configure_fragment_types' => [
        'title' => $this->t('Configure Fragement Types'),
        'description' => $this->t('Configure your fragment types like social media links, global CTAs and reusable content.'),
        'url' => '/admin/structure/fragment_type',
        'link_text' => 'Configure Fragment Types',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M3 10h4v4H3zM17 10h4v4h-4zM10 3h4v4h-4zM10 17h4v4h-4z" />
        </svg>',
      ],
      'configure_area' => [
        'title' => $this->t('Configure Areas'),
        'description' => $this->t('Configure your areas to place fragments in the header, footer or anywhere you need them.'),
        'url' => '/admin/content/area',
        'link_text' => 'Configure Areas',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M6 3h12M6 21h12M3 6v12M21 6v12M6 6h12v12H6z" />
        </svg>',
      ],
      'configure_menus' => [
        'title' => $this->t('Configure Menus'),
        'description' => $this->t('Configure navigation menus with links and attributes.'),
        'url' => '/admin/structure/menu',
        'link_text' => 'Configure Menus',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M3 6h18M3 12h12m-12 6h9" />
        </svg>',
      ],
      'configure_media' => [
        'title' => $this->t('Configure Media Types'),
        'description' => $this->t('Configure your media types.'),
        'url' => '/admin/structure/media',
        'link_text' => 'Configure Media',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M3 6h18M3 12h12m-12 6h9" />
        </svg>',
      ],
      'configure_webforms' => [
        'title' => $this->t('Configure Webforms'),
        'description' => $this->t('Collect data by creating webforms.'),
        'url' => '/admin/structure/webform',
        'link_text' => 'Configure Webforms',
        'icon' => '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
        <path stroke-linecap="round" stroke-linejoin="round" d="M6 4h12a2 2 0 012 2v12a2 2 0 01-2 2H6a2 2 0 01-2-2V6a2 2 0 012-2z" />
        <path stroke-linecap="round" stroke-linejoin="round" d="M9 8h6M9 12h4M9 16h6M4 8h2M4 12h2M4 16h2" />
        </svg>',
      ],

    ];
    return $create_links;
  }

  /**
   * Render the links into a structured array for Drupal rendering system.
   *
   * @param array $links
   *   The links to render.
   *
   * @return array
   *   The renderable array of links.
   */
  function renderLinks($section, $links)
  {
    $build[$section] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['gin-layer-wrapper', 'quick-links', 'quick-links-grid']],
    ];
    foreach ($links as $key => $info) {
      $build[$section][$key] = [
        '#type' => 'fieldset',
        '#title' => $info['title'],
        '#open' => TRUE,
      ];

      $icon_markup = !empty($info['icon']) ? $info['icon'] : '';

      $build[$section][$key]['content'] = [
        '#type' => 'item',
        '#markup' =>
          '<div class="link-content">' .
          '<div class="icon-container">' . $icon_markup . '</div>' .
          '<div>' . $info['description'] . '</div>' .
          '</div>' .
          '<p><a href="' . $info['url'] . '" class="button button--small">' . $info['link_text'] . '</a></p>',

        '#allowed_tags' => ['div', 'p', 'a', 'svg', 'path', 'rect', 'circle'], // Add more allowed tags as needed.
      ];
    }
    return $build;
  }
}
