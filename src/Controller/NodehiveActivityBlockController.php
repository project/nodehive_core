<?php

namespace Drupal\nodehive_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;

/**
 * Controller for rendering node preview in the 'rag' view mode.
 */
class NodehiveActivityBlockController extends ControllerBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a NodehiveActivityBlockController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    DateFormatterInterface $date_formatter,
    AccountProxyInterface $current_user,
    RendererInterface $renderer,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
    $this->currentUser = $current_user;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('current_user'),
      $container->get('renderer'),
    );
  }

  /**
   * Renders a preview of nodes based on the action.
   *
   * @param string $action
   *   The action to determine which nodes to render.
   * @param string $number_of_items
   *   The number of items.
   * @param string $workflow
   *   The workflow.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response containing the rendered content.
   */
  public function renderAction($action, $number_of_items, $workflow) {
    $langcodes = array_keys($this->languageManager()->getLanguages());
    $langcode_header = strtoupper(implode(" ", $langcodes));

    $header = [
      'title' => $this->t('Title'),
      'language' => $langcode_header,
      'status' => $this->t('Status'),
      'workflow_state' => $this->t('Workflow State'),
      'author' => $this->t('Author'),
      'last_edited' => $this->t('Last Edited'),
      'space' => $this->t('Space'),
      'view_link' => '',
      'edit_link' => '',
    ];

    $nodes = ($action === 'assigned-to-me') ? $this->getAssignedToMeContent($number_of_items) : $this->getTeamContent($number_of_items);
    $rows = $this->buildNodeRows($nodes, $workflow);
    $build = [
      '#type' => 'table',
      '#header' => $header,
    ];

    foreach ($rows as $row) {
      $build[] = $row;
    }

    $build['#attached']['library'][] = 'nodehive_core/activity-block';

    return new Response($this->renderer->renderRoot($build));
  }

  /**
   * Builds the rows for the nodes table.
   *
   * @param array $nodes
   *   The nodes to render.
   * @param string $workflow
   *   The workflow.
   *
   * @return array
   *   An array of table rows.
   */
  protected function buildNodeRows(array $nodes, $workflow) {
    $rows = [];
    foreach ($nodes as $node) {
      assert($node instanceof NodeInterface);
      $rows[] = [
        'title' => ['#markup' => $this->buildNodeLink($node)],
        'language' => $this->buildLanguageSwitcher($node),
        'status' => ['#markup' => $this->buildNodeStatus($node)],
        'workflow_state' => ['#markup' => $this->getWorkflowState($node, $workflow)],
        'author' => ['#markup' => $this->getAuthorName($node)],
        'last_edited' => ['#markup' => $this->dateFormatter->formatDiff($node->getChangedTime(), REQUEST_TIME)],
        'space' => ['#markup' => $this->getSpaceLink($node)],
        'view_link' => ['#markup' => $this->buildLink($node, 'view')],
        'edit_link' => ['#markup' => $this->buildLink($node, 'edit')],
      ];
    }
    return $rows;
  }

  /**
   * Builds a link for a node.
   */
  protected function buildNodeLink($node) {
    $node_url = Url::fromRoute('entity.node.canonical', ['node' => $node->id()]);
    $node_link = Link::fromTextAndUrl($node->label(), $node_url)->toString();
    return $node_link;
  }

  /**
   * Builds a status for a node.
   */
  protected function buildNodeStatus($node) {
    $status = $node->isPublished() ? $this->t('Published') : $this->t('Unpublished');
    $markup = "<span class='marker marker--" . strtolower($status) . "'>$status</span>";
    return $markup;
  }

  /**
   * Builds a language switcher for a node.
   */
  protected function buildLanguageSwitcher($entity) {
    $build = [];
    $langcodes = array_keys($this->languageManager()->getLanguages());

    foreach ($langcodes as $langcode) {
      if ($entity->hasTranslation($langcode)) {
        $build['translation_link'][$langcode] = [
          '#title' => 'Edit ' . $langcode . ' translation',
          '#type' => 'link',
          '#url' => $entity->getTranslation($langcode)->toUrl('edit-form'),
          '#attributes' => [
            'class' => [
              $langcode . '-has-translation', 'language-has-translation',
            ],
            'title' => 'Edit ' . $langcode . ' translation',
          ],
        ];
      }
      else {
        $build['translation_link'][$langcode] = [
          '#title' => 'Add ' . $langcode . ' translation',
          '#type' => 'link',
          '#url' => Url::fromRoute('entity.' . $entity->getEntityTypeId() . '.content_translation_add', [
            'source' => $entity->language()->getId(),
            'target' => $langcode,
            $entity->getEntityTypeId() => $entity->id(),
          ]),
          '#attributes' => [
            'class' => [
              'language-add-translation',
            ],
            'title' => 'Add ' . $langcode . ' translation',
          ],
        ];
      }
    }

    $build['translation_link']['#attached']['library'][] = 'nodehive_core/views.entity.translations.links';

    return $build;
  }

  /**
   * Builds a link for a node.
   */
  protected function buildLink($node, $type = 'view') {
    $route = ($type === 'edit') ? 'entity.node.edit_form' : 'entity.node.canonical';
    return Link::fromTextAndUrl($this->t(ucfirst($type)), Url::fromRoute($route, ['node' => $node->id()]))->toString();
  }

  /**
   * Gets the workflow state for a node.
   */
  protected function getWorkflowState($node, $workflow) {
    $state = '';
    if ($node->hasField('moderation_state') && !$node->get('moderation_state')->isEmpty()) {
      $workflow = $this->entityTypeManager->getStorage('workflow')->load($workflow);
      $state_plugin = $workflow ? $workflow->getTypePlugin()->getState($node->get('moderation_state')->value) : NULL;
      $state = $state_plugin ? $state_plugin->label() : '';
    }

    $markup = "<span class='marker marker--" . strtolower($state) . "'>$state</span>";
    return $markup;
  }

  /**
   * Gets the author's name for a node.
   */
  protected function getAuthorName($node) {
    $author_name = '';
    if ($node->hasField('uid') && !$node->get('uid')->isEmpty()) {
      $author = $this->entityTypeManager->getStorage('user')->load($node->get('uid')->target_id);
      $author_name = $author ? $author->getDisplayName() : '';
    }
    return $author_name;
  }

  /**
   * Gets the space link for a node.
   */
  protected function getSpaceLink($node) {
    if ($node->hasField('nodehive_space') && !$node->get('nodehive_space')->isEmpty()) {
      $space = $this->entityTypeManager->getStorage('nodehive_space')->load($node->get('nodehive_space')->target_id);
      return $space ? Link::fromTextAndUrl($space->label(), Url::fromRoute('entity.nodehive_space.canonical', ['nodehive_space' => $space->id()]))->toString() : $this->t('not set');
    }
    return $this->t('not set');
  }

  /**
   * Retrieves nodes assigned to the current user.
   */
  protected function getAssignedToMeContent($number_of_items) {
    return $this->loadNodes($number_of_items, ['uid' => $this->currentUser->id()]);
  }

  /**
   * Retrieves team nodes.
   */
  protected function getTeamContent($number_of_items) {
    return $this->loadNodes($number_of_items);
  }

  /**
   * Loads nodes with optional conditions.
   */
  protected function loadNodes($number_of_items, array $conditions = []) {
    $query = $this->entityTypeManager->getStorage('node')->getQuery()->accessCheck(TRUE)->sort('changed', 'DESC')->range(0, $number_of_items);
    foreach ($conditions as $field => $value) {
      $query->condition($field, $value);
    }
    return $this->entityTypeManager->getStorage('node')->loadMultiple($query->execute());
  }

}
