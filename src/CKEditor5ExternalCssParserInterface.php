<?php

namespace Drupal\nodehive_core;

/**
 * Provides an interface for CKEditor5ExternalCSSParser.
 */
interface CKEditor5ExternalCssParserInterface {

  /**
   * Parses the external CSS file and appends CKEditor 5 classes.
   *
   * @param string $url
   *   External CSS url.
   */
  public function parseCss(string $url);

}
