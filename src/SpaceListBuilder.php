<?php

namespace Drupal\nodehive_core;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the space entity type.
 */
class SpaceListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new NodehiveSpaceListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      "No spaces available, please <a href='@link'>create a new space.</a>",
      [
        '@link' => Url::fromUserInput('/space/add')->toString(),
      ]
    );

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total spaces: @total', ['@total' => $total]);
    $build['summary']['#weight'] = 0;
    $build['table']['#weight'] = 1;
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['type'] = $this->t('Space Type');
    $header['url'] = $this->t('Url');
    $header['users'] = $this->t('# Users');
    $header['status'] = $this->t('Status');
    $header['created'] = $this->t('Created');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $type = NULL;
    if ($entity->space_type->value) {
      $type_values = nodehive_core_space_type_allowed_values(
        $entity->space_type->getFieldDefinition(),
        $entity,
        TRUE
      );

      $type = isset($type_values[$entity->space_type->value]) ? $type_values[$entity->space_type->value] : "";
    }

    // Space url.
    $space_url = Url::fromUri($entity->space_url->uri);
    // Get number of users.
    $editors = $entity->editors->referencedEntities();

    /** @var \Drupal\nodehive_core\NodehiveSpaceInterface $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->toLink();
    $row['type'] = $type;
    $row['url'] = Link::fromTextAndUrl($entity->space_url->uri, $space_url)->toString();
    $row['users'] = count($editors);
    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $row['created'] = $this->dateFormatter->format($entity->get('created')->value);
    return $row + parent::buildRow($entity);
  }

}
