<?php

namespace Drupal\nodehive_area_fragment\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting an area.
 */
class AreaDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $space_id = $this->getRequest()->query->get('nodehive_space');
    if (!$space_id) {
      return parent::submitForm($form, $form_state);
    }

    $space = $this->entityTypeManager
      ->getStorage("nodehive_space")
      ->load($space_id);

    Cache::invalidateTags($this->entity->getCacheTags());
    Cache::invalidateTags($space->getCacheTags());

    return parent::submitForm($form, $form_state);
  }

}
