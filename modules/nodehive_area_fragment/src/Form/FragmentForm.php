<?php

namespace Drupal\nodehive_area_fragment\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the fragment edit forms.
 */
class FragmentForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $space_id = $this->getRequest()->query->get('nodehive_space');

    if ($space_id && $this->operation == 'add') {

      $space = $this->entityTypeManager
        ->getStorage("nodehive_space")
        ->load($space_id);

      if ($space) {
        $form['space_id']['widget']['#default_value'] = [$space->id()];
      }
    }

    $form['bundle'] = [
      '#type' => 'item',
      '#title' => $this->t(
        "Type: @bundle",
        [
          '@bundle' => $this->entity->bundle(),
        ]
      ),
      '#weight' => -10,
    ];

    $form['uid']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $space_id = $this->getRequest()->query->get('nodehive_space');
    $space = NULL;

    if ($space_id) {
      $space = $this->entityTypeManager->getStorage("nodehive_space")->load($space_id);
    }

    if ($this->operation == "add") {
      $uid = $this->currentUser()->id();
      $this->entity->uid = ['target_id' => $uid];
    }

    $status = parent::save($form, $form_state);

    $entity = $this->entity;
    if ($status == SAVED_UPDATED) {
      $this->messenger()
        ->addMessage($this->t('The fragment %feed has been updated.', ['%feed' => $entity->toLink()->toString()]));
    }
    else {
      $this->messenger()
        ->addMessage($this->t('The fragment %feed has been added.', ['%feed' => $entity->toLink()->toString()]));
    }

    Cache::invalidateTags($this->entity->getCacheTags());

    $referenced_spaces = $this->entity->space_id->referencedEntities();
    foreach ($referenced_spaces as $ref_space) {
      Cache::invalidateTags($ref_space->getCacheTags());
    }

    if ($space) {
      $form_state->setRedirect(
        'entity.fragment.space_collection',
        [
          "nodehive_space" => $space_id,
        ],
      );
    }
    else {
      $form_state->setRedirect(
        'entity.nodehive_fragment.collection',
        [
          "nodehive_fragment" => $this->entity->id(),
        ],
      );
    }
  }

}
