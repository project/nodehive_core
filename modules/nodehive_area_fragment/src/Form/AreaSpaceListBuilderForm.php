<?php

namespace Drupal\nodehive_area_fragment\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for removing a fragment from a area.
 */
class AreaSpaceListBuilderForm extends SpaceListBuilderFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nodehive_area_fragment_area_space_list_builder_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $build = parent::buildForm($form, $form_state);
    $nodehive_space_id = $this->getRouteMatch()->getParameter('nodehive_space');
    $nodehive_space = $this->entityTypeManager
      ->getStorage('nodehive_space')
      ->load($nodehive_space_id);

    $header = $this->listBuilderHelper->buildAreaHeader();
    $header['operations'] = $this->t("Operations");

    $build['areas'] = [
      '#type' => 'table',
      '#header' => $header,
    ];

    $entity_ids = $this->listBuilderHelper->getAreaEntityIds([
      'space_id' => $nodehive_space->id(),
    ]);

    foreach ($entity_ids as $area) {
      if ($row = $this->listBuilderHelper->buildAreaRow($area)) {
        // Add destination to label url.
        /** @var \Drupal\Core\Url $url */
        $url = $row['label']['data']['#url'];
        $url = $this->ensureDestination($url, $nodehive_space);
        $row['label']['data']['#url'] = $url;

        $row[] = [
          'data' => $this->buildOperations($area, $nodehive_space),
        ];
        $build['areas']['#rows'][$area->id()] = $row;
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
