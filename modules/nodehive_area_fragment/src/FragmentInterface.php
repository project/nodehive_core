<?php

namespace Drupal\nodehive_area_fragment;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Fragment entities.
 */
interface FragmentInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, RevisionLogInterface, EntityPublishedInterface {

  /**
   * Gets the fragment type.
   *
   * @return string
   *   The fragment type.
   */
  public function getType();

  /**
   * Get the title.
   *
   * @return string
   *   Entity title.
   */
  public function getTitle();

  /**
   * Sets the title.
   *
   * @param string $title
   *   The Recently read name.
   *
   * @return \Drupal\nodehive_area_fragment\FragmentInterface
   *   The called entity.
   */
  public function setTitle($title);

  /**
   * Gets the creation timestamp.
   *
   * @return int
   *   Creation timestamp/
   */
  public function getCreatedTime();

  /**
   * Sets the creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return \Drupal\nodehive_area_fragment\FragmentInterface
   *   The called entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the fragment revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the fragment revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return $this
   *   The called node entity.
   */
  public function setRevisionCreationTime($timestamp);

}
