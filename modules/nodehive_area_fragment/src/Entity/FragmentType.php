<?php

namespace Drupal\nodehive_area_fragment\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Fragment Type.
 *
 * @ConfigEntityType(
 *   id = "nodehive_fragment_type",
 *   label = @Translation("Fragment Type"),
 *   bundle_of = "nodehive_fragment",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "nodehive_fragment_type",
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\nodehive_area_fragment\Form\FragmentTypeEntityForm",
 *       "add" = "Drupal\nodehive_area_fragment\Form\FragmentTypeEntityForm",
 *       "edit" = "Drupal\nodehive_area_fragment\Form\FragmentTypeEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\nodehive_area_fragment\FragmentTypeEntityListBuilder",
 *   },
 *   admin_permission = "administer nodehive fragment type",
 *   links = {
 *     "canonical" = "/admin/structure/fragment_type/{nodehive_fragment_type}",
 *     "add-form" = "/admin/structure/fragment_type/add",
 *     "edit-form" = "/admin/structure/fragment_type/{nodehive_fragment_type}/edit",
 *     "delete-form" = "/admin/structure/fragment_type/{nodehive_fragment_type}/delete",
 *     "collection" = "/admin/structure/fragment_type",
 *   }
 * )
 */
class FragmentType extends ConfigEntityBundleBase {

  /**
   * The machine name of this node type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the node type.
   *
   * @var string
   */
  protected $label;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

}
