<?php

namespace Drupal\nodehive_area_fragment\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionLogEntityTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\nodehive_area_fragment\AreaInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Area entity.
 *
 * @ContentEntityType(
 *   id = "nodehive_area",
 *   label = @Translation("Area"),
 *   base_table = "nodehive_area",
 *   entity_keys = {
 *     "id" = "fid",
 *     "uuid" = "uuid",
 *     "revision" = "vid",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *     "space_id" = "space_id",
 *     "published" = "status",
 *     "label" = "label",
 *   },
 *   handlers = {
 *     "list_builder" = "Drupal\nodehive_area_fragment\AreaListBuilder",
 *     "access" = "Drupal\nodehive_area_fragment\AreaAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\nodehive_area_fragment\Form\AreaForm",
 *       "add" = "Drupal\nodehive_area_fragment\Form\AreaForm",
 *       "edit" = "Drupal\nodehive_area_fragment\Form\AreaForm",
 *       "delete" = "Drupal\nodehive_area_fragment\Form\AreaDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/area/add",
 *     "delete-form" = "/area/{nodehive_area}/delete",
 *     "canonical" = "/area/{nodehive_area}",
 *     "revision" = "/area/{nodehive_area}/revisions/{nodehive_area_revision}/view",
 *     "edit-form" = "/area/{nodehive_area}/edit",
 *     "collection" = "/admin/content/area",
 *   },
 *   admin_permission = "administer nodehive area",
 *   collection_permission = "access area overview",
 *   revision_table = "nodehive_area_revision",
 *   revision_data_table = "nodehive_area_field_revision",
 *   data_table = "nodehive_area_field_data"
 * )
 */
class Area extends ContentEntityBase implements ContentEntityInterface, AreaInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use RevisionLogEntityTrait;

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->get('label')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($label) {
    $this->set('label', $label);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['machine_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Machine name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setRevisionable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['space_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('NodeHive Space'))
      ->setDescription(t('The space, that this content will appear on.'))
      ->setSetting('target_type', 'nodehive_space')
      ->setSetting('handler', 'default')
      ->setTargetEntityTypeId('nodehive_space')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 5,
        'settings' => [],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['allowed_fragment_types'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Fragment types'))
      ->setDescription(t('The allowed fragment types.'))
      ->setSetting('target_type', 'nodehive_fragment_type')
      ->setSetting('handler', 'default')
      ->setTargetEntityTypeId('nodehive_space')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 5,
        'settings' => [],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['fragment_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Fragments'))
      ->setDescription(t('The fragments.'))
      ->setSetting('target_type', 'nodehive_fragment')
      ->setSetting('handler', 'default')
      ->setTargetEntityTypeId('nodehive_space')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The area language code.'));

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the area was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the area was last edited.'));

    return $fields;
  }

}
