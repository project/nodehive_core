<?php

namespace Drupal\nodehive_area_fragment;

/**
 * Provides an interface for list builder helper.
 */
interface AreaFragmentListBuilderHelperInterface {

  /**
   * Build area sortable table headers.
   *
   * @return array
   *   Render array.
   */
  public function buildAreaHeader();

  /**
   * Build area table row.
   *
   * @param \Drupal\nodehive_area_fragment\AreaInterface $area
   *   Area entity.
   *
   * @return array
   *   Render array.
   */
  public function buildAreaRow(AreaInterface $area);

  /**
   * Get all area entities.
   *
   * @param array $conditions
   *   Additional conditions to apply to the query.
   *
   * @return array
   *   Render array.
   */
  public function getAreaEntityIds(array $conditions = []);

  /**
   * Build fragment sortable table headers.
   *
   * @return array
   *   Render array.
   */
  public function buildFragmentHeader();

  /**
   * Build fragment table row.
   *
   * @param \Drupal\nodehive_area_fragment\FragmentInterface $fragment
   *   Fragment entity.
   *
   * @return array
   *   Render array.
   */
  public function buildFragmentRow(FragmentInterface $fragment);

  /**
   * Get all fragment entities.
   *
   * @param array $conditions
   *   Additional conditions to apply to the query.
   *
   * @return array
   *   Render array.
   */
  public function getFragmentEntityIds(array $conditions = []);

}
