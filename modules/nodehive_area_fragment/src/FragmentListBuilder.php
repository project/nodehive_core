<?php

namespace Drupal\nodehive_area_fragment;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the fragment entity type.
 */
class FragmentListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The list builder helper.
   *
   * @var \Drupal\nodehive_area_fragment\AreaFragmentListBuilderHelperInterface
   */
  protected $listBuilderHelper;

  /**
   * Constructs a new FragmentListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\nodehive_area_fragment\AreaFragmentListBuilderHelperInterface $list_builder_helper
   *   The list builder helper.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    DateFormatterInterface $date_formatter,
    LanguageManagerInterface $language_manager,
    AreaFragmentListBuilderHelperInterface $list_builder_helper
  ) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->languageManager = $language_manager;
    $this->listBuilderHelper = $list_builder_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('language_manager'),
      $container->get('nodehive_area_fragment.list_builder_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = $this->listBuilderHelper->buildFragmentHeader();
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    return $this->listBuilderHelper->getFragmentEntityIds();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = $this->listBuilderHelper->buildFragmentRow($entity);
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = \Drupal::formBuilder()->getForm('Drupal\nodehive_area_fragment\Form\FragmentListBuilderForm');
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if (isset($operations['delete'])) {
      unset($operations['delete']['attributes']['class']);
      unset($operations['delete']['attributes']['data-dialog-type']);
      unset($operations['delete']['attributes']['data-dialog-options']);
    }

    return $operations;
  }

}
