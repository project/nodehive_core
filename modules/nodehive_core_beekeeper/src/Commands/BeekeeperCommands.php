<?php

namespace Drupal\nodehive_core_beekeeper\Commands;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\nodehive_core_beekeeper\BeekeeperInterface;
use Drush\Commands\DrushCommands;

/**
 * Provides methods to install nodehive_core functionality.
 */
class BeekeeperCommands extends DrushCommands {
  use StringTranslationTrait;

  /**
   * The beekeeper service.
   *
   * @var \Drupal\nodehive_core_installer\BeekeeperInterface
   */
  protected $beekeeper;

  /**
   * BeekeeperCommands constructor.
   *
   * @param \Drupal\nodehive_core_installer\BeekeeperInterface $beekeeper
   *   The beekeeper service.
   */
  public function __construct(BeekeeperInterface $beekeeper) {
    $this->beekeeper = $beekeeper;
  }

  /**
   * Install beekeeper preset.
   *
   * @command beekeeper:install
   *
   * @aliases bi
   */
  public function install(
    array $options = [
      'preset' => 'nodehive',
    ]
  ) {
    $this->output()->writeln(
      $this->t("Installing modules and config for @preset...",
      [
        "@preset" => $options['preset'],
      ])
    );

    drupal_flush_all_caches();
    $this->executeDrushCommands($options);

    drupal_flush_all_caches();
    $this->installMenu($options);
    $this->installPermissions($options);
    $this->installDefaultConfig($options);
  }

  /**
   * Execute Drush commands for a specific preset.
   *
   * @command beekeeper:drush:install
   *
   * @aliases bdi
   */
  public function executeDrushCommands(
    array $options = [
      'preset' => 'nodehive',
    ]
  ) {
    $this->output()->writeln("Executing Drush commands...");
    $this->beekeeper->executeDrushCommands($options['preset']);
  }

  /**
   * Install custom menus via Drush.
   *
   * @command beekeeper:menu:install
   *
   * @aliases bmi
   */
  public function installMenu(
    array $options = [
      'preset' => 'nodehive',
    ]
  ) {
    $this->output()->writeln("Installing Drupal menus ...");
    $this->beekeeper->installDrupalMenu($options['preset']);
    $this->output()->writeln("Installing Drupal menus DONE");
  }

  /**
   * Install permissions via Drush.
   *
   * @command beekeeper:permissions:install
   *
   * @aliases bpi
   */
  public function installPermissions(
    array $options = [
      'preset' => 'nodehive',
    ]
  ) {
    $this->output()->writeln("Installing Drupal permissions ...");
    $this->beekeeper->installDrupalPermissions($options['preset']);
    $this->output()->writeln("Installing Drupal permissions DONE");
  }

  /**
   * Install default Drupal config.
   *
   * @command beekeeper:default-config:install
   *
   * @aliases bdci
   */
  public function installDefaultConfig(
    array $options = [
      'preset' => 'nodehive',
    ]
  ) {
    $this->output()->writeln("Installing Drupal config ...");
    $this->beekeeper->installDrupalConfig($options['preset']);
    $this->output()->writeln("Installing Drupal config DONE");
  }

}
