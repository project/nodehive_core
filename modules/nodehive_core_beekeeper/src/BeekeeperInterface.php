<?php

namespace Drupal\nodehive_core_beekeeper;

/**
 * Interface for Beekeeper class.
 */
interface BeekeeperInterface {

  /**
   * Executes Drush commands for specific preset.
   *
   * @param string $preset
   *   Preset type.
   */
  public function executeDrushCommands(string $preset): void;

  /**
   * Installs Drupal menu for specific preset.
   *
   * @param string $preset
   *   Preset type.
   */
  public function installDrupalMenu(string $preset): void;

  /**
   * Installs Drupal permissions for specific preset.
   *
   * @param string $preset
   *   Preset type.
   */
  public function installDrupalPermissions(string $preset): void;

  /**
   * Installs Drupal config for specific preset.
   *
   * @param string $preset
   *   Preset type.
   */
  public function installDrupalConfig(string $preset): void;

}
