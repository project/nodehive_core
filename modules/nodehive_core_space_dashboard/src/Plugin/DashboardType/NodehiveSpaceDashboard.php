<?php

namespace Drupal\nodehive_core_space_dashboard\Plugin\DashboardType;

use Drupal\content_planner\DashboardBlockPluginManager;
use Drupal\content_planner\DashboardTypeBase;
use Drupal\content_planner\DashboardTypeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a space dashboard type.
 *
 * @DashboardType(
 *   id = "nodehive_space_dashboard",
 *   label = @Translation("Nodehive Space Dashboard"),
 *   canonical_route = "entity.nodehive_space.canonical",
 *   base_route = "/space/{nodehive_space}/dashboard",
 *   entity_type = "nodehive_space",
 *   base_route_parameters = {
 *     "nodehive_space"
 *   }
 * )
 */
class NodehiveSpaceDashboard extends DashboardTypeBase implements DashboardTypeInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a NodehiveSpaceDashboard object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\content_planner\DashboardBlockPluginManager $dashboard_block_plugin_manager
   *   The dashboard block plugin manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Menu\LocalTaskManagerInterface $local_task_manager
   *   The local task manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $destination
   *   The destination service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    DashboardBlockPluginManager $dashboard_block_plugin_manager,
    AccountProxyInterface $current_user,
    LocalTaskManagerInterface $local_task_manager,
    RouteMatchInterface $route_match,
    RedirectDestinationInterface $destination,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $config_factory,
      $dashboard_block_plugin_manager,
      $current_user,
      $local_task_manager,
      $route_match,
      $destination
    );
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('content_planner.dashboard_block_plugin_manager'),
      $container->get('current_user'),
      $container->get('plugin.manager.menu.local_task'),
      $container->get('current_route_match'),
      $container->get('redirect.destination'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPageTitle(string $dashboard_id): string {
    $space_id = $this->getRouteMatch()->getParameter(
      'nodehive_space'
    );

    $space = $this->entityTypeManager
      ->getStorage("nodehive_space")
      ->load($space_id);

    return $space->label();
  }

}
