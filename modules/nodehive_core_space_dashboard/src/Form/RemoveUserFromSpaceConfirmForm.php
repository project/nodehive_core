<?php

namespace Drupal\nodehive_core_space_dashboard\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirm form removing a user from space.
 */
class RemoveUserFromSpaceConfirmForm extends ConfirmFormBase {

  /**
   * The space entity.
   *
   * @var \Drupal\nodehive_core\SpaceInterface
   */
  protected $space;

  /**
   * The user entity.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a RemoveUserFromSpaceConfirmForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entity_type_manager,
    RouteMatchInterface $route_match
  ) {
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;

    $space = NULL;
    if ($space_id = $this->routeMatch->getParameter('nodehive_space')) {
      /** @var \Drupal\nodehive_core\SpaceInterface|null $space */
      $space = $this->entityTypeManager
        ->getStorage('nodehive_space')
        ->load($space_id);
    }

    if (!$space) {
      throw new \Exception("Missing nodehive_space entity.");
    }

    $this->space = $space;

    $user = NULL;
    if ($user_id = $this->routeMatch->getParameter('user')) {
      /** @var \Drupal\user\UserInterface|null $user */
      $user = $this->entityTypeManager
        ->getStorage('user')
        ->load($user_id);

      if (!$user) {
        throw new \Exception("Missing user entity.");
      }

      $this->user = $user;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nodehive_core_space_dasboard_remove_user_from_space_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
      'Are you sure you want remove %user from space %name?',
      [
        '%user' => $this->user->getAccountName(),
        '%name' => $this->space->label(),
      ],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t(
      "This will remove a user from a space and all access to the content used in space."
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.nodehive_space.canonical', ['nodehive_space' => $this->space->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $saved_values = $this->space->editors->getValue();
    $new_values = [];

    foreach ($saved_values as $item) {
      if ($item['target_id'] != $this->user->id()) {
        $new_values[] = $item['target_id'];
      }
    }

    $this->space->editors = $new_values;
    $this->space->save();

    $this->messenger->addStatus(
      "User removed from space successfully."
    );

    $this->redirect(
      'nodehive_core_space_dashboard.users_list_builder',
      [
        'nodehive_space' => $this->space->id(),
      ],
    );
  }

}
