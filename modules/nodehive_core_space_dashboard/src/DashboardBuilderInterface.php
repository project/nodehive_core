<?php

namespace Drupal\nodehive_core_space_dashboard;

use Drupal\nodehive_core\SpaceInterface;

/**
 * Provides an interface for dashboard builder class.
 */
interface DashboardBuilderInterface {

  /**
   * Build the 'Add content' section.
   *
   * @param \Drupal\nodehive_core\SpaceInterface $space
   *   Space entity.
   *
   * @return array
   *   Render array.
   */
  public function buildAddContentBlock(SpaceInterface $space): array;

  /**
   * Build the 'Menu' section.
   *
   * @param \Drupal\nodehive_core\SpaceInterface $space
   *   Space entity.
   *
   * @return array
   *   Render array.
   */
  public function buildMenuBlock(SpaceInterface $space): array;

  /**
   * Build the 'Content' section.
   *
   * @param \Drupal\nodehive_core\SpaceInterface $space
   *   Space entity.
   *
   * @return array
   *   Render array.
   */
  public function buildContentBlock(SpaceInterface $space): array;

}
