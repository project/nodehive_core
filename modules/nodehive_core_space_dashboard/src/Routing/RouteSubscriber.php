<?php

namespace Drupal\nodehive_core_space_dashboard\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route = $collection->get('entity.nodehive_space.canonical');
    if ($route) {
      $route->addDefaults(['_controller' => '\Drupal\nodehive_core_space_dashboard\Controller\SpaceDashboardController::dashboardRender']);
    }
  }

}
