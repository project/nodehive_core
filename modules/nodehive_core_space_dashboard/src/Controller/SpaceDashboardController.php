<?php

namespace Drupal\nodehive_core_space_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\nodehive_core\SpaceInterface;
use Drupal\nodehive_core_space_dashboard\DashboardBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Space dashboard.
 */
class SpaceDashboardController extends ControllerBase {

  /**
   * The dashboard builder.
   *
   * @var \Drupal\nodehive_core_space_dashboard\DashboardBuilderInterface
   */
  protected $dashboardBuilder;

  /**
   * Constructs a new SpaceDashboardController object.
   *
   * @param \Drupal\nodehive_core_space_dashboard\DashboardBuilderInterface $dashboard_builder
   *   The dashboard builder.
   */
  public function __construct(DashboardBuilderInterface $dashboard_builder) {
    $this->dashboardBuilder = $dashboard_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nodehive_core_space_dashboard.dashboard_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function dashboardRender(SpaceInterface $nodehive_space) {
    $build = [];

    /*
    $build['top_content'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['top-container'],
      ],
    ];

    $build['top_content']['add_content_block'] = $this->dashboardBuilder->buildAddContentBlock($nodehive_space);

    $build['middle_content'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ["middle-content-container"],
      ],
    ];

    $build['middle_content']['menu_block'] = $this->dashboardBuilder->buildMenuBlock($nodehive_space);

    $build['middle_content']['content_block'] = $this->dashboardBuilder->buildContentBlock($nodehive_space);
    */
    $build['content_block'] = $this->dashboardBuilder->buildContentBlock($nodehive_space);
    $build['#attached']['library'][] = 'nodehive_core_space_dashboard/dashboard';

    return $build;
  }

}
