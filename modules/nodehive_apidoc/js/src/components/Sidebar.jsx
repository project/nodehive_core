import React from 'react';

function Sidebar({ data, setContent }) {

  const setContentById = (id, items) => {
    for (const key in items) {
      if (items[key].Id === id) {
        const content = items[key].Contents;
        setContent(content);
      }
      if (typeof items[key] === 'object') {
        setContentById(id, items[key]);
      }
    }
  };

  const handleClick = (e) => {
    let id = e.target.dataset.id;
    setContentById(id, data)
  };

  const renderNav = (items, isNested = false) => {
    return (
      <ul className={`list-none pl-3 pr-3`}>
        {Object.keys(items).map((item) => {
          if (!items[item].hasOwnProperty('Contents')) {
            return (
              <li key={items[item].id} className={`mb-2 ${isNested ? 'pl-1' : ''}`}>
                <p
                  className={`block pt-3 pb-3 font-bold`}
                >
                  {item}
                </p>
                {typeof items[item] === 'object' && renderNav(items[item], true)}
              </li>
            );
          }

          return (
            <li key={items[item].id} className={`mb-2 ${isNested ? 'pl-1' : ''}`}>
              <a
                onClick={handleClick}
                href='#'
                data-id={`${items[item].Id}`}
                className={`block pt-3 pb-3`}
              >
                {item}
              </a>
            </li>
          );
        })}
      </ul>
    );
  };

  return (
    <div className="min-w-fit w-1/12 bg-gray-100 max-h-full shadow-inner">
      {renderNav(data)}
    </div>
  );
}

export default Sidebar;
