import React from 'react';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import ReactMarkdown from 'react-markdown';
import Code from './Code';

function Content({ jsonData }) {
  const CodeBlock = ({ language, value }) => {
    return (
      <SyntaxHighlighter language='javascript' style={okaidia}>
        {value}
      </SyntaxHighlighter>
    );
  };

  const renderContent = (data) => {
    return (
      <ReactMarkdown
        children={data}
        components={{
          code(props) {
            const {children, className, node, ...rest} = props
            return (
              <Code codeExample={children} float={false} />
            );
          }
        }}
      />
    );

  };

  return <div className="pl-5 pr-5 pb-10 w-full rounded-md doc-content ">{renderContent(jsonData)}</div>;
}

export default Content;
