// Deconstructed
import React, { Component, Fragment } from 'react';

function Layout({children}) {
  return (
    <div className="bg-white shadow-xl w-full">
      {children}
    </div>
  );
}

export default Layout;
