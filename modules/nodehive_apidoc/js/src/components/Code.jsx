import React, {useState} from 'react';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import ClipboardJS from 'clipboard';

function Code({codeExample, float}) {
  const copyToClipboard = () => {
    const clipboard = new ClipboardJS('.copy-button', {
      text: () => codeExample,
    });

    clipboard.on('success', (e) => {
      e.clearSelection();
    });
  };

  if (!codeExample) {
    return (
      <>
      </>
    );
  }

  return (
    <div className={`pl-5 pr-5 w-2/12 code-example ${float ? 'pt-20' : 'mt-10 mb-10'}`}>
      <div className="relative">
        <div className={`p-4 text-center p-5 bg-gray-200 rounded-lg border shadow-xl ${float ? 'fixed is-float' : 'w-fit'}`}>
          <h2 className="font-bold">Javascript</h2>
          <button
            className="copy-button absolute top-2 left-2 bg-teal-600 text-white px-2 py-2 rounded"
            onClick={copyToClipboard}
          >
            Copy
          </button>
          <div className="">
            <SyntaxHighlighter language='javascript' style={okaidia}>
              {codeExample}
            </SyntaxHighlighter>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Code;
