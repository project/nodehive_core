import React, { useEffect, useState, useRef } from 'react';
import Layout from "./components/Layout"
import Sidebar from "./components/Sidebar"
import Content from "./components/Content"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';

function App() {
  const containerRef = useRef(null);
  const [documentation, setDocumentation] = useState([]);
  const [apiUrl, setApiUrl] = useState(drupalSettings.nodehive_apidoc.api_url);
  const [codeExample, setCodeExample] = useState('');
  const [blockQuotes, setBlockQuotes] = useState([]);
  const [content, setContent] = useState('');

  const setContentDataFromChild = (data) => {
   // Remove existing elements with the class 'response-wrapper'
    const existingResponseWrappers = containerRef.current.querySelectorAll('.response-wrapper');
    existingResponseWrappers.forEach((wrapper) => {
      wrapper.remove();
    });

    setContent(data);
  };

  const transformKeysAndValues = (jsonData) => {
    const transformedData = {};

    for (const key in jsonData) {
      if (jsonData.hasOwnProperty(key)) {
        const transformedKey = key
          .split('-')
          .map(word => word.charAt(0).toUpperCase() + word.slice(1))
          .join(' ');

        if (typeof jsonData[key] === 'object') {
          transformedData[transformedKey] = transformKeysAndValues(jsonData[key]);
        } else {
          transformedData[transformedKey] = jsonData[key];
        }
      }
    }

    return transformedData;
  }

  const fetchDocumentation = async () => {
    try {
      const response = await fetch(apiUrl);
      if (!response.ok) {
        throw new Error('Error occurred while fetching documentation.');
      }
      const jsonData = await response.json();

      setDocumentation(transformKeysAndValues(jsonData));

    } catch (error) {
      console.error('Error fetching documentation:', error);
    }
  };

  useEffect(() => {
    if (documentation.length <= 0) {
      fetchDocumentation();
    }

    const blockQuotesWithUrls = Array.from(containerRef.current.querySelectorAll('blockquote p'));
    // Execute the URLs and replace block content with actual results
    const fetchAndReplaceContent = async () => {
      const updatedBlockQuotes = [];

      for (const blockQuote of blockQuotesWithUrls) {
        const url = blockQuote.innerHTML;
        try {
         // Remove existing elements with the class 'response-wrapper'
          const existingResponseWrappers = containerRef.current.querySelectorAll('.response-wrapper');
          existingResponseWrappers.forEach((wrapper) => {
            wrapper.remove();
          });

          const response = await fetch(url);
          const data = await response.text();
          const dataParsed = JSON.parse(data);

          const codeElement = document.createElement('pre');
          codeElement.classList = "response bg-black text-white w-full pl-2 pt-2 pb-2";
          codeElement.textContent = JSON.stringify(dataParsed, null, 2);

          // Create a parent div element and append the code element to it
          const parentDiv = document.createElement('div');
          parentDiv.classList = "response-wrapper p-4 p-5 bg-gray-200 rounded-lg border shadow-xl w-fit mt-10";
          parentDiv.appendChild(codeElement);

          const blockquoteParent = blockQuote.parentElement;
          blockquoteParent.parentNode.appendChild(parentDiv, blockquoteParent);
          updatedBlockQuotes.push(codeElement);
        } catch (error) {
          console.error(`${error}`);
        }
      }
    };

    fetchAndReplaceContent();
  });

  return (
    <Layout>
      <div className="pt-5 pb-5 text-center bg-gray-100">
        <h1 className="text-lg font-bold">Nodehive Documentation</h1>
      </div>
      <div className="flex border max-h-full" ref={containerRef}>
        <Sidebar data={documentation} setContent={setContentDataFromChild} />
        <Content jsonData={content} />
      </div>
    </Layout>
  );
}

export default App;
