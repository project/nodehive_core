## Introduction

This page shows examples of various GET requests for the JSON:API module.

In all examples below, no request headers are required. No authentication is required if anonymous users can access entities.

Note that in all cases, when an id is needed, it is always the entity's uuid, not the entity id.
