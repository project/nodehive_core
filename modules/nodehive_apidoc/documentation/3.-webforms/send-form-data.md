### Introduction

Send form data from a frontend to NodeHive.

You may need to adapt permissions so that anonymous users can use /webform_rest/submit

### Example code

```
  const data = {
    "webform_id": "webform_id",
    "e_mail": email,
    "your_form_element_id": your_value
  }

  const payload = JSON.stringify(data);
  console.log(payload);

  const res = await fetch(`${process.env.NODEHIVE_URL}/webform_rest/submit`, {
    method: 'POST',
    body: payload,
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  });

  const response = await res.json()
```
