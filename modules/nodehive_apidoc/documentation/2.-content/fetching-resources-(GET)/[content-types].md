# [nodehive_apidoc:node--first-title]

## Endpoint

URL: [nodehive_apidoc:base-url]/jsonapi/node/[type]/[content-uuid]

### Code:

```
async function getContent() {
  const response = await fetch("[nodehive_apidoc:base-url]/jsonapi/node/[nodehive_apidoc:node--first-type]/[nodehive_apidoc:node--first-uuid]");
  const content = await response.json();
  console.log(content);
}
```

### Response:


>[nodehive_apidoc:response:nodehive_apidoc:base-url/jsonapi/node/nodehive_apidoc:node--first-type/nodehive_apidoc:node--first-uuid]
