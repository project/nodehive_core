<?php

namespace Drupal\nodehive_apidoc\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for the API documentation.
 */
class ApiDocController extends ControllerBase {

  /**
   * Extension module list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Constructs a new ApiDocController.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   Module extension list.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   * @param \Drupal\Core\Utility\Token $token
   *   Token service.
   */
  public function __construct(ModuleExtensionList $module_extension_list, RequestStack $request_stack, Token $token) {
    $this->moduleExtensionList = $module_extension_list;
    $this->requestStack = $request_stack;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.list.module'),
      $container->get('request_stack'),
      $container->get('token')
    );
  }

  /**
   * Controller to build the wrapper for React component.
   *
   * @return array
   *   The render array.
   */
  public function build() {
    $build = [];

    $build['react_app'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'react-api-doc',
      ],
    ];

    $build['#attached']['library'][] = 'nodehive_apidoc/react_doc';

    $scheme_and_http_host = $this->requestStack
      ->getCurrentRequest()
      ->getSchemeAndHttpHost();

    $url_api_docs = Url::fromRoute('nodehive_apidoc.api_docs')->toString();

    $build['#attached']['drupalSettings']['nodehive_apidoc']['api_url'] =
      $scheme_and_http_host . '' . $url_api_docs;

    return $build;
  }

  /**
   * Get all available documentation.
   *
   * @return array
   *   The render array.
   */
  public function getDocumentation() {
    $root = $this->moduleExtensionList->getPath('nodehive_apidoc');
    $documentation_path = $root . '/documentation';
    $files = $this->scanDirectory($documentation_path);

    return new JsonResponse($files);
  }

  /**
   * Get all available documentation.
   *
   * @return array
   *   The render array.
   */
  protected function scanDirectory($dir) {
    $result = [];
    $files = @scandir($dir);

    foreach ($files as $file) {
      if ($file == '.' || $file == '..') {
        continue;
      }

      $path = $dir . '/' . $file;
      if (is_dir($path)) {
        $result[$file] = $this->scanDirectory($path);
      }
      else {
        $result = $this->processFile($result, $dir, $file);
      }
    }

    return $result;
  }

  /**
   * Process a file and update the result array.
   *
   * @param array $result
   *   The result array to be updated.
   * @param string $dir
   *   The directory path.
   * @param string $file
   *   The file name.
   *
   * @return array
   *   The updated result array.
   */
  protected function processFile($result, $dir, $file) {
    $contents = file_get_contents($dir . '/' . $file);
    $file = str_replace(".md", "", $file);

    if (preg_match('/^\[.*\]$/', $file)) {
      $items = $this->replaceFilenameTokens($file);
      $result = $this->processItems($result, $dir, $file, $contents, $items);
    }
    else {
      $contents = $this->token->replace($contents);
      $result[str_replace(".md", "", $file)] = [
        'id' => str_replace(".md", "", $file),
        'filepath' => $dir . '/' . $file,
        'contents' => $contents,
      ];
    }

    return $result;
  }

  /**
   * Process items and update the result array.
   *
   * @param array $result
   *   The result array to be updated.
   * @param string $dir
   *   The directory path.
   * @param string $file
   *   The file name.
   * @param string $contents
   *   The file contents.
   * @param array $items
   *   An array of items to be processed.
   *
   * @return array
   *   The updated result array.
   */
  protected function processItems($result, $dir, $file, $contents, $items) {
    foreach ($items as $key => $item) {
      if ($item instanceof EntityInterface) {
        $item_contents = $this->token->replace($contents, ['entity' => $item]);
        if ($this->hasNodehiveApidocTokens($item_contents)) {
          $item_contents = $this->t("## No content found\n\r")->__toString();
          $item_contents .= $this->t("Please create a new content of this type and then the documentation will be auto-generated.");
        }

        $result[$item->label()] = [
          'id' => $item->id(),
          'filepath' => $dir . '/' . $file,
          'contents' => $item_contents,
        ];
      }
      else {
        $result[$item] = [
          'id' => $key,
          'filepath' => $dir . '/' . $file,
          'contents' => $contents,
        ];
      }
    }

    return $result;
  }

  /**
   * Get all available documentation.
   *
   * @param string $token
   *   The token string.
   *
   * @return string
   *   Replaced token.
   */
  protected function replaceFilenameTokens($token) {
    $result = NULL;

    switch ($token) {
      case '[content-types]':
        /** @var \Drupal\node\NodeTypeInterface[] $ct_types */
        $ct_types = $this->entityTypeManager()->getStorage('node_type')->loadMultiple();
        foreach ($ct_types as $ct_type) {
          $result[$ct_type->id()] = $ct_type;
        }
        break;
    }

    return $result;
  }

  /**
   * Detects if a text contains [nodehive_apidoc:] token.
   *
   * @param string $text
   *   The text to check.
   *
   * @return bool
   *   Returns TRUE if the text contains the token.
   */
  protected function hasNodehiveApidocTokens($text) {
    $pattern = '/\[nodehive_apidoc:([^]]+)\]/';
    return preg_match($pattern, $text) === 1;
  }

}
