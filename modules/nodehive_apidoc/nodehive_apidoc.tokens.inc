<?php

/**
 * @file
 * Builds placeholder replacement tokens for nodehive_apidoc-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function nodehive_apidoc_token_info() {

  $types['nodehive_apidoc'] = [
    'name' => t('Nodehive API doc'),
    'description' => t('Tokens related to nodehive api documentation.'),
  ];

  $nodehive_apidoc['list-content-types'] = [
    'name' => t('List Content Types'),
    'description' => t("Lists all content types."),
  ];

  return [
    'types' => $types,
    'tokens' => ['nodehive_apidoc' => $nodehive_apidoc],
  ];
}

/**
 * Implements hook_tokens().
 */
function nodehive_apidoc_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'nodehive_apidoc') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'list-content-types':
          $list = "";

          /** @var \Drupal\node\NodeTypeInterface[] $ct_types */
          $ct_types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
          foreach ($ct_types as $ct_type) {
            $list .= "* [" . $ct_type->label() . "](#" . $ct_type->id() . ")\n";
          }
          $list .= "";

          $replacements[$original] = t("@list", ["@list" => $list]);
          break;

        case 'base-url':
          $replacements[$original] = \Drupal::request()->getSchemeAndHttpHost();
          break;

        case 'node--first-type':
          if (!$data['entity']) {
            break;
          }

          // Get first published node of this type.
          $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
            'status' => 1,
            'type' => $data['entity']->id(),
          ]);

          if (!$nodes) {
            return;
          }
          $node = reset($nodes);

          $replacements[$original] = $node->bundle();
          break;

        case 'node--first-uuid':
          if (!$data['entity']) {
            break;
          }

          // Get first published node of this type.
          $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
            'status' => 1,
            'type' => $data['entity']->id(),
          ]);

          if (!$nodes) {
            return;
          }
          $node = reset($nodes);

          $replacements[$original] = $node->uuid();
          break;

        case 'node--first-title':
          if (!$data['entity']) {
            break;
          }

          // Get first published node of this type.
          $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
            'status' => 1,
            'type' => $data['entity']->id(),
          ]);

          if (!$nodes) {
            return;
          }
          $node = reset($nodes);

          $replacements[$original] = $data['entity']->label();
          break;
      }

      // Response.
      if (str_contains($name, "response:")) {
        $name = str_replace("response:", "", $name);
        $url_parts = explode("/", $name);
        foreach ($url_parts as $key => $part) {
          if (isset($data['entity'])) {
            $url_parts[$key] = \Drupal::token()->replace("[" . $part . "]", ['entity' => $data['entity']]);
          }
          else {
            $url_parts[$key] = \Drupal::token()->replace("[" . $part . "]");
          }
        }

        $url = implode("/", $url_parts);
        $url = str_replace("[", "", $url);
        $url = str_replace("]", "", $url);

        $replacements[$original] = $url;
      }
    }
  }

  return $replacements;
}
