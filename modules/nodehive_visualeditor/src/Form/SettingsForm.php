<?php

namespace Drupal\nodehive_visualeditor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Visual Editor.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nodehive_visualeditor_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nodehive_visualeditor.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nodehive_visualeditor.settings');

    $form['development_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable development mode'),
      '#description' => $this->t('Add a development site to the visual editor selector.'),
      '#default_value' => $config->get("development_mode"),
    ];

    $form['development_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Development url'),
      '#description' => $this->t('Link to the development site url.'),
      '#default_value' => $config->get("development_url"),
      '#states'   => [
        'visible' => [
          ':input[name="development_mode"][value="1"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('nodehive_visualeditor.settings');

    $config->set("development_mode", $form_state->getValue("development_mode"));
    $config->set("development_url", $form_state->getValue("development_url"));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
