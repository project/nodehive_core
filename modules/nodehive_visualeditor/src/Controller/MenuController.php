<?php

namespace Drupal\nodehive_visualeditor\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides a Controller to edit menu.
 */
class MenuController extends ControllerBase {

  /**
   * Load the form for a menu to edit.
   */
  public function menuEdit($menu_id) {
    $storage = "menu";
    $display = "edit";

    $fragment = $this->entityTypeManager()
      ->getStorage('menu')
      ->load($menu_id);

    $formObject = $this->entityTypeManager()->getFormObject($storage, $display);
    $formObject->setEntity($fragment);
    $form = $this->formBuilder()->getForm($formObject);

    return $form;
  }


}
