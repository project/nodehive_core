<?php

namespace Drupal\nodehive_visualeditor\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides a Controller to edit Fragments.
 */
class AreaFragmentController extends ControllerBase {

  /**
   * Load the form for a paragraph to edit.
   */
  public function fragmentEdit($fragment_id) {
    $storage = "nodehive_fragment";
    $display = "edit";

    $fragment = $this->entityTypeManager()
      ->getStorage('nodehive_fragment')
      ->load($fragment_id);

    $formObject = $this->entityTypeManager()->getFormObject($storage, $display);
    $formObject->setEntity($fragment);
    $form = $this->formBuilder()->getForm($formObject);

    return $form;
  }

   /**
   * Load the form for a paragraph to edit.
   */
  public function areaEdit($area_id) {
    $storage = "nodehive_area";
    $display = "edit";

    $fragment = $this->entityTypeManager()
      ->getStorage('nodehive_area')
      ->load($area_id);

    $formObject = $this->entityTypeManager()->getFormObject($storage, $display);
    $formObject->setEntity($fragment);
    $form = $this->formBuilder()->getForm($formObject);

    return $form;
  }
}
