<?php

namespace Drupal\nodehive_visualeditor\Controller;

use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\TempStore\PrivateTempStoreFactory;


/**
 * Defines the VisualEditorController class.
 */
class VisualEditorController extends ControllerBase
{

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack|null
   */
  protected $requestStack;

  /**
   * The form builder.
   *
   * @var FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The private temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Constructs a new VisualEditorController object.
   *
   * @param RequestStack $request_stack
   *   The request stack.
   * @param FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(RequestStack $request_stack, FormBuilderInterface $form_builder, PrivateTempStoreFactory $temp_store_factory)
  {
    $this->requestStack = $request_stack;
    $this->formBuilder = $form_builder;
    $this->tempStoreFactory = $temp_store_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('request_stack'),
      $container->get('form_builder'),
      $container->get('tempstore.private'),
    );
  }

  /**
   * Will show the first space available or the space selected in the tempstore.
   *
   * @return array
   *   Return a renderable array for the visual editor page.
   */
  public function visualEditorPage()
  {
    $spaces = $this->entityTypeManager()
      ->getStorage("nodehive_space")
      ->loadByProperties([
        'status' => 1,
      ]);

    if (!$spaces) {
      return [
        '#theme' => 'inline_template',
        '#markup' => $this->t(
          "No spaces available, please <a href='@link'>create a new space.</a>",
          [
            '@link' => Url::fromUserInput('/space/add')->toString(),
          ]
        )
      ];
    } else {
      $store = $this->tempStoreFactory->get('nodehive_tempstore');
      $space_id = $store->get('nodehive_space') ?: '';

      if (!$space_id) {
        $space_id = array_key_first($spaces);
      }

      $url = Url::fromRoute('nodehive_visualeditor.visual_editor_single', ['nodehive_space' => $space_id])->toString();
      return new \Symfony\Component\HttpFoundation\RedirectResponse($url);
    }
  }

  /**
   * Display the visual editor page for a single space.
   *
   * @return array
   *   Return a renderable array for the visual editor page.
   */
  public function visualEditorSingleSpacePage($nodehive_space)
  {

    // Get all published spaces.
    $spaces = $this->entityTypeManager()
      ->getStorage("nodehive_space")
      ->loadByProperties([
        'status' => 1,
      ]);

    if (!$spaces) {
      $this->messenger()->addWarning(
        $this->t(
          "No spaces available, please <a href='@link'>create a new space.</a>",
          [
            '@link' => Url::fromUserInput('/space/add')->toString(),
          ]
        )
      );
    }

    // Load current selected space
    $space_entity = $this->entityTypeManager()
      ->getStorage('nodehive_space')
      ->load($nodehive_space);

    $url_from_get = $this->requestStack->getCurrentRequest()->query->get("url");
    if ($url_from_get) {
      $url = $url_from_get;
    } else {
      /** @var \Drupal\node\NodeInterface $frontpage_node */
      $frontpage_node = $space_entity->get('frontpage_node')->entity;
      if ($frontpage_node) {
        $url = $space_entity->space_url[0]->uri . '/node/' . $frontpage_node->id();
      } else {
        $url = $space_entity->space_url[0]->uri;
      }
    }

    $base_url = $space_entity->space_url[0]->uri;
    $recent_edit_form = $this->formBuilder->getForm('Drupal\nodehive_visualeditor\Form\RecentEditForm', $nodehive_space);





    // Build the rest of your render array.
    $build['content'] = [
      '#theme' => 'nodehive_visualeditor',
      '#recent_edit_form' => $recent_edit_form,
      '#spaces' => $spaces,
      '#selected_space' => $space_entity->toArray(),
      '#selected_space_id' => $nodehive_space,
      '#url' => $url,
      '#frontend_login_url' => $space_entity->frontpage_login_url[0]->uri,
      '#base_url' => $base_url,
      '#attached' => [
        'drupalSettings' => [
          'nodehive_visualeditor' => [
            'is_multilingual' => $this->isSiteMultilingual(),
          ],
        ],
      ],
      '#cache' => ['max-age' => 0],
    ];
    return $build;
  }

  /**
   * Helper method to get the space title.
   *
   * @return string|null
   *   Return string or null if no label found.
   */
  public function getTitle($nodehive_space)
  {
    $space_entity = $this->entityTypeManager()
      ->getStorage('nodehive_space')
      ->load($nodehive_space);

    return $space_entity->label();
  }

  /**
   * Checks if the site is configured with multiple languages.
   *
   * @return bool
   *   Returns true if the site is multilingual, false otherwise.
   */
  protected function isSiteMultilingual()
  {
    $languages = \Drupal::languageManager()->getLanguages();
    return count($languages) > 1;
  }

}
