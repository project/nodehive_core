<?php

namespace Drupal\nodehive_visualeditor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Provides a Controller to edit Paragraphs.
 */
class NodeController extends ControllerBase
{

  /**
   * Load the form for a paragraph to edit.
   */
  public function nodeEdit($node_id)
  {
    $storage = "node";
    $display = "composable";

    $node = $this->entityTypeManager()
      ->getStorage('node')
      ->load($node_id);

    $formObject = $this->entityTypeManager()->getFormObject($storage, $display);
    $formObject->setEntity($node);
    $form = $this->formBuilder()->getForm($formObject);
    $menu = [
      [
        'label' => $node->type->entity->label(),
      ],
    ];

    $menu = [
      [
        'label' => $node->type->entity->label(),
      ],
    ];

    return [
      '#theme' => 'off_canvas',
      '#form' => $form,
      '#menu' => $menu,
    ];
  }


}
