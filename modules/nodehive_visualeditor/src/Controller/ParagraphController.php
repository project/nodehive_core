<?php

namespace Drupal\nodehive_visualeditor\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides a Controller to edit Paragraphs.
 */
class ParagraphController extends ControllerBase {

  /**
   * Load the form for a paragraph to edit.
   */
  public function paragraphEdit($node_id, $paragraph_id) {
    $storage = "paragraph";
    $display = "default";

    $paragraph = $this->entityTypeManager()
      ->getStorage('paragraph')
      ->load($paragraph_id);

    if (!$paragraph) {
      $build['wrapper']['label'] = [
        '#type' => 'label',
        '#title' => $this->t("Please select a paragraph."),
      ];
      return;
    }

    $formObject = $this->entityTypeManager()->getFormObject($storage, $display);
    $formObject->setEntity($paragraph);
    $form = $this->formBuilder()->getForm($formObject);

    // Due to an issue with delete link route, we are not allowing to delete
    // the translation directly from visual editor.
    unset($form['actions']['delete_translation']);

    return $form;
  }

}
